var webpack = require('webpack')
var FlowStatusWebpackPlugin = require('flow-status-webpack-plugin')
var precss = require('precss')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var atImport = require('postcss-import')
var bemLinter = require('postcss-bem-linter')
// postcss plugins
const cssimport = require('postcss-import')
const customProperties = require('postcss-custom-properties')
const autoprefixer = require('autoprefixer-core')
const csswring = require('csswring')
const cssnested = require('postcss-nested')

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: './src/react/entry.js',


  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: [
            require.resolve('babel-preset-es2015'),
            require.resolve('babel-preset-react'),
            require.resolve('babel-preset-stage-0'),
          ],
      },
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader?limit=32768'
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ],
    plugins: [
      // new webpack.HotModuleReplacementPlugin(),
      // hot reload
      // new webpack.IgnorePlugin(/\.json$/),
      new webpack.DefinePlugin({
        __DEVELOPMENT__: false,
        __QA__: true,
        __DEVTOOLS__: false,
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new FlowStatusWebpackPlugin(),
      new ExtractTextPlugin('styles.css')
    ]
  },
  resolve: {
    extensions: ['', '.react.js', '.js', '.jsx', '.css'],
    modulesDirectories: [
      'src', 'node_modules'
    ]
  },
  postcss: function () {
    return [cssimport, precss, customProperties, autoprefixer, csswring, bemLinter, cssnested, atImport({
      path: ['node_modules', './src']
    })]
  }
}
