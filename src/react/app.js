/*
** This file is the entry into the app
** List all different components needed here
**
*/
// require('../config/router')
import 'babel-polyfill'
import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import { Router, Route, hashHistory } from 'react-router'

console.debug(' app kickstarted ')
const projectName = 'test'
const moduleName = 'test-container'
import { TestContainer } from './views/test/test-container'

export function attach(uidocument) {
  render((
    <div>
      <Router history={hashHistory}>
        <Route path='*' component={TestContainer} />
      </Router>
    </div>
  ), uidocument.getElementById('app'))
}

export function detach(uidocument) {
  unmountComponentAtNode(uidocument.getElementById('app'))
}
