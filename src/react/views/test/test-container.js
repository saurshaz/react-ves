import './test-container.css'
import React from 'react'
import {TestNavi} from './test-navi'
import {TestMain} from './test-main'
import {TestHelp} from './test-help'

export const TestContainer = React.createClass({

  componentDidMount() {
    // let self = this;
    // for validations
    //  - add a validationform
    //  - add a validate function in the handlers['<tag-name>'] section to set result
    //                into state.<store>.<validationform>.validated property
    // self.stores = ['user'] // stores to watch
    // self.validationform = [] // validation form . this will have validated field
    //
    // self.mixin(StoreWatcher)
    // self.mixin(RouterX)
  },

  render() {
    return (
      <section id='main-container'>
        <TestMain />
        <TestNavi />
        <TestHelp />
      </section>
    )
  }
})
