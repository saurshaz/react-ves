import './test-help.css'
import React from 'react'

export const TestHelp = React.createClass({
  getInitialState() {
    return {'help': 'All your queries answered here'}
  },
  render() {
    return (
      <section id='help'>
        <p>{this.state && this.state.help || 'Generic help'}</p>
      </section>
    )
  }
})
