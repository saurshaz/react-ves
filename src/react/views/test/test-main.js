import './test-main.css'
import React from 'react'
const StoreWatcher = require('../../../event-manager/utils/storex')
const Authx = require('../../../event-manager/utils/authx')
const RouterX = require('../../../event-manager/utils/routerx')
import { render, unmountComponentAtNode } from 'react-dom'

export const TestMain = React.createClass({
  componentDidMount() {
    let self = this
    self.setState({
      'a': 'aaa', isFirst: true,
      body: 'here\'s the body', title: 'sample title', data: [
      { id: 'apple', title: 'Apple', body: 'The world biggest fruit company.' },
      { id: 'orange', title: 'Orange', body: 'I don\'t have the word for it...' }
      ]})
    // for validations
    //  - add a validationform
    //  - add a validate function in the handlers['<tag-name>'] section to set result
    //                into state.<store>.<validationform>.validated property
    self.identifier = 'TestMain'
    // stores to watch
    self.stores = ['user']
    // validation form . this will have validated field
    self.validationform = []

    StoreWatcher.init(self)

    Authx.init(self)

    RouterX.init(self, (target, fragment, options) => {
      // render imperatively
      render(target, document.querySelector(fragment), options)
    })
  },
  render() {
    let liContent
    if (this.state && this.state.data) {
      liContent = (this.state && this.state.data && this.state.data.map(function (result, cnt) {
        return <li key={cnt}><a href='#/first/{ result.id }'>{result.title}</a></li>
      }))
    }
    return (
      <section id='content-area'>
        <article>
          <h1>{this.state && this.state.title}</h1>
          <p>{this.state && this.state.body}</p>
          <ul if={this.state && this.state.isFirst}>
            {liContent}
          </ul>
        </article>
      </section>
    )
  }
})
