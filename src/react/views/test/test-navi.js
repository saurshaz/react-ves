import './test-navi.css'
import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
const StoreWatcher = require('../../../event-manager/utils/storex')
const Authx = require('../../../event-manager/utils/authx')
const RouterX = require('../../../event-manager/utils/routerx')
export const TestNavi = React.createClass({
  componentDidMount() {
    // for validations
    //  - add a validationform
    //  - add a validate function in the handlers['<tag-name>'] section to set result
    //                into state.<store>.<validationform>.validated property
    this.identifier = 'TestNavi'
    // stores to watch
    this.stores = ['counter']
    // validation form . this will have validated field
    this.validationform = []

    StoreWatcher.init(this)

    Authx.init(this)

    RouterX.init(this, (target, fragment, options) => {
      // render imperatively
      const Counter =  require('../common/'+fragment).default
      // let Appbrowser = new modules()
      // render(React.createElement("Counter"), options.domElement)
      let domTarget = options.domElement
      delete options.domElement
      render(<Counter {...options} />, domTarget)
      // @todo make it dynamic so that it can load Counter or Appbrowser based on fragment value
      // line 25 n 28 have hardcoding
    })
  },
  navTo(e){
    if (e.target.nodeName === 'A' && e.target.classList.contains('__trigger')){
      this._.navTo(e.target.getAttribute('data-link'),'test',e.target.parentElement.parentElement.querySelector('section#content-area'))
    }
  },
  render() {
    
    return (
      <section id='links'>
        <a onClick={this.navTo} className='__trigger' data-link='?page=appbrowser&target=section#content-area&fragment=Appbrowser'>A</a>
        <a onClick={this.navTo} className='__trigger' data-link='?page=counter&domain=counter&target=section#content-area&fragment=Counter'>C</a>
        <a onClick={this.navTo} className='__trigger' data-link='?page=counter&domain=counter&target=section#content-area&fragment=Counter'>{this.state && this.state.counter.cnt}</a>
      </section>
    )
  }
})
