import React, { PropTypes } from 'react'
const StoreWatcher = require('../../../event-manager/utils/storex')
const Authx = require('../../../event-manager/utils/authx')
const RouterX = require('../../../event-manager/utils/routerx')
// export const Counter = connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(clicker)

const Counter = React.createClass({
  propTypes: {
    domain: PropTypes.string,
    page: PropTypes.string,
    _: PropTypes.object.isRequired,
  },
  componentWillMount(){
    // for validations
    //  - add a validationform
    //  - add a validate function in the handlers['<tag-name>'] section to set result
    //                into state.<store>.<validationform>.validated property
    this.identifier = 'counter'
    // stores to watch
    this.stores = ['counter']
    // validation form . this will have validated field
    this.validationform = []
    StoreWatcher.init(this)
    // RouterX.init(this, (target, fragment, options) => { console.log('initialized routing for ',this.identifier)})
  },
  render() {
    // const handler = page
    console.log('counter >> ',this.state)
    return (
      <div>
        <span style={{color: 'cyan'}}>You have clicked {this.state && this.state.counter && this.state.counter.cnt} times</span>
        <button style={{'backgroundColor': 'green'}} id='btnIncrement' onClick={this.props._.increment}>
          +1
        </button>
        <button style={{'backgroundColor': 'red'}} id='btnDecrement' onClick={this.props._.decrement}>
          -1
        </button>
      </div>
    )
  }
})

export default Counter