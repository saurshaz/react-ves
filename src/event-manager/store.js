'use strict'
let PubSub = require('./pubsub.js')

// todo :: restrict property access without `setState`
let state = {
  global: {}, fyler: {request: '', requestjson: {}, response: '', err: ''},
  user: {loggedin: 'false', applerac: '', authStatus: false, userId: '',
  userPassword: '', loginform: {validated: false}}, misc: {},
  counter: {'cnt':0}
}

module.exports = (() => {
  return {
    init: function () {
      return state
    },
    setState: function (module, key, val) {
      // @todo : :check that it handles nested tree updates to state
      state[module][key] = val
      console.log('>> state is -> ', state)
      PubSub.publish(module + '_updated', {module: module, key: key, val: val, state: state})
    },
    getState: function (module, key) {
      // @todo : :check that it handles nested tree fetches from state
      if (!module || !key) {
        return null
      }
      return state[module][key]
    },
    register: function (storeNameKey, val) {
      let arrState = []
      if (typeof storeNameKey !== 'object') {
        arrState.push(storeNameKey)
      }

      arrState.map((stateItem, i) => {
        // make the change in needed JSOn and then emit an update change for that store
        let module = stateItem.split('.')[0]
        let key = stateItem.split('.')[1]
        state[module] = state[module] || {}
        state[module][key] = val
      })
    }
  }
})()
