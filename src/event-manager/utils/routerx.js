// @todo :: fixme :: rm hardcoding. 2 lines below
const handlers = require('../handlers')

const _VIEW_DIR = '../../react/views/test/'
const _MODULES_EXTENSION = '.html'

module.exports = {
  // init method is a special one which can initialize
  // the mixin when it's loaded to the tag and is not
  // accessible from the tag its mixed in
  nextView: 'xxx',
  init: function (self, cb) {
    // self._ shall already be existing, if not skip
    if (!self._) {
      self._ = {}
    }
    self._.navTo = function (targetUri, container, targetInDom) {
      this.nextView = container
      if (targetUri.indexOf('target') !== -1) {
        let extraParams = {domain: '', page: '', view: '', target: '', fragment: ''}
        let replaced = targetUri.slice(1)
        let arr = replaced.split('&')
        arr.map((item, i) => {
          let keyValArr = item.split('=')
          extraParams[keyValArr[0]] = keyValArr[1] || ''
        })

        let options = {
          domain: extraParams.domain,
          page: extraParams.page,
          domElement: targetInDom,
          _:{},
        }

        // @todo :: make the function called on domain instead of page
        cb(extraParams.target, extraParams.fragment, options)
      } else {
        document.location.href = targetUri
      }
    }
  }
}
