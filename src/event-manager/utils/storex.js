'use strict'
const handlers = require('../handlers')
const store = require('../store')
const PubSub = require('../pubsub')

module.exports = {
  // init method is a special one which can initialize
  // the mixin when it's loaded to the tag and is not
  // accessible from the tag its mixed in
  init: function (self) {
    // this.on('updated', function () { console.log('Updated!') })
    self.state = store.init()
    self._ = self.state
    let identifier = self.identifier
    // / setup all events
    // PubSub.publish('setup_all_events', {context: self})

    // todo : try to get this stores from init param only
    self.stores.map((thiStore, i) => {
      // add handler functions as properties to functions
      if (handlers[identifier] && Object.keys(handlers[identifier]) && self.props._){
        Object.keys(handlers[identifier] && handlers[identifier]).forEach((func, k) => {
          self.props._[func] = handlers[identifier][func].bind(this,{ page: identifier, domain: self.props._.domain}, store, null, null)
        })
      }
 
        //  todo :: validation needed shall be fetched from module name
        // debugger
       PubSub.subscribe(thiStore + '_updated', (data) => {
          // self.validationform.map((validationItem) => {
          //   if (data.key === validationItem && data.val.validated && data.val.validated === true) {
          //     PubSub.publish(identifier + '_setup_events', {context: self})
          //   } else if (data.key === validationItem &&
          //       data.val.validated && data.val.validated !== true) {
          //     PubSub.publish(identifier + '_destroy_events', {context: self})
          //   }
          // })
          console.debug(' storewatcher :: update data >>> ', data)
          self.setState(data)
          // self.update()
      })
    })

    if (identifier && handlers[identifier] && handlers[identifier].onmount &&
      (typeof handlers[identifier].onmount === 'function')) {
      handlers[identifier].onmount.call(self,
        {page: identifier, domain: self.opts.domain}, store, null, null)
    }

    self.validate = () => {
      if (identifier && handlers[identifier] && handlers[identifier].validate &&
       (typeof handlers[identifier].validate === 'function')) {
        handlers[identifier].validate.call(self,
          {page: identifier, domain: self.opts.domain}, store, null, null)
      }
    }
  }
}
