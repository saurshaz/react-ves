'use strict;'

import handlers from './handlers'
import store from './store'
import PubSub from './pubsub'

let eventsConfig = {}


eventsConfig.counter = []
eventsConfig.counter.push({
  selector: {
    nodename: 'BUTTON',
    nodeid: 'btnIncrement'
  },
  event: 'click',
  signal: 'increment',
  handler: 'increment'
})

eventsConfig.counter.push({
  selector: {
    nodename: 'BUTTON',
    nodeid: 'btnDecrement'
  },
  event: 'click',
  signal: 'decrement',
  handler: 'decrement'
})

// serveauth components config
eventsConfig.test = []
eventsConfig.test.push({
  signal: 'auth-done',
  handler: 'onAuthDone'
})

function setupEvents(data) {
  let context = data.context
  // handle riot and react @ todo switch case later
  context.opts = context.opts || context.props 
  let domain = context.opts.domain || data.domain
  let page = context.opts.page || data.page
  console.debug('setupEvents::  domain > ', domain, ' page > ', page)
  if (domain && page) {
    for (let idx in eventsConfig[page]) {
      let eventJson = eventsConfig[page][idx]
      let handler = eventJson.handler

      if (eventJson.event && eventJson.signal) {
        PubSub.publish(eventJson.signal, {context : context, data: data})
      }else if (!eventJson.event && eventJson.signal) {
        PubSub.subscribe(eventJson.signal, (data) => {
          handlers[page][handler].call(context._, {page: page, domain: domain, data: data}, store, (err, result) => {
            context.update()
            console.log('err -> ', err, ' result-> ', context._)
          })
        })
      } else {
        context.root = context.root || document
        context.root.addEventListener(eventJson.event, (e) => {
          if (e.target.nodeName === eventJson.selector.nodename && e.target.id === eventJson.selector.nodeid) {
            handlers[page][handler].call(context._, {page: page, domain: domain}, store, (err, result) => {
              context.update()
              console.log('err -> ', err, ' result-> ', context._)
            })
          }
        })
      }
    }
  }
}

function destroyEvents(data) {
  let context = data.context
  let domain = context.opts.domain
  let page = context.opts.page
  if (domain && page) {
    for (let idx in eventsConfig[page]) {
      let eventJson = eventsConfig[page][idx]
      let handler = eventJson.handler
      context.root.removeEventListener(eventJson.event) // todo :: test
      context.update()
    }
  }
}

function setupAllEventHooks(data) {
  let element = data.context.identifier ||  data.context.root.getAttribute('data-is')
  // PubSub.subscribe(element + '_setup_events', (data) => {
  setupEvents(data)
// })
// PubSub.subscribe(element + '_destroy_events', (data) => {
//   destroyEvents(data)
// })
}

PubSub.subscribe('setup_all_events', (data) => {
  setupAllEventHooks(data)
})

module.exports = eventsConfig
